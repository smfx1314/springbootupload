package com.jiangfeixiang.springbootupload.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author jiangfeixiang
 * @ClassName UploadController.java
 * @email 1016767658@qq.com
 * @Description TODO
 * @createTime 2020年06月26日 11:09:00
 */
@RestController
public class UploadController {

    @RequestMapping(value = "/fileupload",method = RequestMethod.POST)
    public String fileUpload(MultipartFile file) throws IOException {//前台表单中name值是什么就填写什么，这里name=file
        //获得原文件名称
        String originalname = file.getOriginalFilename();
        System.out.println(originalname);
        //上传指定文件：在桌面upload文件夹中
        file.transferTo(new File("C:\\Users\\smfx1314\\Desktop\\upload\\"+originalname));
        return "ok";
    }
}
